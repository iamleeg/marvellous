# Marvellous

Display a list of comics from the Marvel API. Tap on any comic to see more detail.

## How to run

If you open the Xcode project you will find that you are missing a file `marvellous/MarvelAPI.xcconfig`. You should create that file and add your API key from developer.marvel.com here:

```
MARVEL_API_PUBLIC = public key
MARVEL_API_PRIVATE = private key
```

Then you can build and run the project from xcode/xcodebuild as required. If you use CD, generate this `xcconfig` file from secrets in your CD workflow.

## Some notes

This was written in about five hours over a small period of days. There is plenty I would do if I thought this was going to be a "product" that I have not done, because I don't think that. Some highlights:

 - iterate on the design. I would work with design and user experience experts on that: particularly the information architecture.
 - using ETag for cache control.
 - saving JSON responses and images to local storage.
 - keying saved images by a hash of their URI to deduplicate fetches.
 - UI testing. I would create UI Automation tests that work off accessibility identifiers. However, this introduces a complexity in that the app downloads fresh data from the API when it launches; we don't therefore have stable data for the test. Either the app could use a VCR-style saved response in test mode, or it could connect to a small local server that replays a controlled response.
 - separate the navigation details from the UI definition.
 - bring all of the networking code into one place, share a queue, set QoS on background work, etc.
