//
//  ContentView.swift
//  marvellous
//
//  Created by Graham Lee on 22/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct ContentView: View {
    @StateObject var collection: ComicCollectionPresentation
    var body: some View {
        GeometryReader { g in
            NavigationView {
                ScrollView {
                    List(collection.comics) { comic in
                        NavigationLink(destination: ComicDetailView(comic: comic)) {
                            ComicView(comic: comic)
                        }
                    }.frame(width: g.size.width - 5,
                            height: g.size.height - 50,
                            alignment: .center)
                    AcknowledgeMarvel()
                        .padding()
                }
                .navigationTitle("Marvel Comics")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let firstComic = ComicModel.previewModel()
        let secondComic = ComicModel(title: "Ant-Man (2003) #2",
                                     thumbnail: ComicThumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/f/20/4bc69f33cafc0", extension: "jpg"),
                                     id: 323,
                                     series: ComicSeries(name: "Ant-Man (2003 - 2004)"))
        let collection = ComicCollectionModel(data: ComicCollectionModel.InternalData(results: [
            firstComic,
            secondComic])
        )
        ContentView(collection: ComicCollectionPresentation(comics: collection))
    }
}
