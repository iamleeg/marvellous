//
//  ComicView.swift
//  marvellous
//
//  Created by Graham Lee on 25/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

extension ComicPresentation {
    var image: Image {
        if let data = imageData {
            if let uiImage = UIImage(data:data) {
                return Image(uiImage: uiImage)
            }
        }
        return Image(systemName: "questionmark.circle.fill")
    }
}

struct ComicView: View {
    @StateObject var comic: ComicPresentation

    var body: some View {
        HStack {
            comic.image.resizable().aspectRatio(contentMode: .fit)
            Text(comic.title)
            Image(systemName: "arrowtriangle.right.fill")
        }
    }
}

struct ComicView_Previews: PreviewProvider {
    static var previews: some View {
        let model = ComicModel.previewModel()
        ComicView(comic: ComicPresentation(model: model))
    }
}
