//
//  ComicModel.swift
//  marvellous
//
//  Created by Graham Lee on 25/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import Foundation

struct ComicThumbnail: Decodable {
    let path: String
    let `extension`: String
}

struct ComicSeries: Decodable {
    let name: String
}

struct ComicModel : Decodable, Identifiable {
    let title: String
    let thumbnail: ComicThumbnail
    let id: Int
    let series: ComicSeries
    
    static func decodeModel(from json: Data) throws -> ComicModel {
        return try JSONDecoder().decode(ComicModel.self, from: json)
    }
    
    static func previewModel() -> ComicModel {
        ComicModel(title: "Marvel Previews (2017)",
                   thumbnail: ComicThumbnail(path: "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available", extension: "jpg"),
                   id: 0,
                   series: ComicSeries(name: "Marvel Previews (2017 - Present)"))
    }
}
