//
//  ComicPresentation.swift
//  marvellous
//
//  Created by Graham Lee on 25/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

class ComicPresentation : ObservableObject, Identifiable {
    @Published private var model = ComicModel(title: "",
                                              thumbnail: ComicThumbnail(path: "", extension: ""),
                                              id:0,
                                              series: ComicSeries(name: ""))
    var title: String {
        model.title
    }
    var thumbnail: URL? {
        let location = "\(model.thumbnail.path).\(model.thumbnail.extension)"
        return URL(string: location)
    }
    @Published var imageData: Data? = nil
    var id: Int {
        model.id
    }
    var seriesName: String {
        model.series.name
    }
    init(model: ComicModel) {
        self.model = model
    }
}
