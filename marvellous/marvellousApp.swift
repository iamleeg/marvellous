//
//  marvellousApp.swift
//  marvellous
//
//  Created by Graham Lee on 22/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI
import CryptoKit

extension ComicPresentation {
    func fetchThumbnail() -> Void {
        guard let thumbnailURL = self.thumbnail else {
            return
        }
        let task = URLSession.shared.dataTask(with: thumbnailURL) { data, response, error in
            if let imageData = data {
                DispatchQueue.main.async {
                    self.imageData = imageData
                }
            }
        }
        task.resume()
    }
}

extension ComicCollectionPresentation {
    
    struct APIError : Error {
        let message: String
        
        init(_ msg: String) {
            message = msg
        }
    }
    
    func infoParameter(_ key: String) -> String? {
        return (Bundle.main.infoDictionary?[key] as? String)
    }
    
    func fetchComicsList() throws -> Void {
        /*
         * The Marvel API private key isn't allowed in source. Here I've configured both
         * private and public keys from Info.plist, and they're set in an .xcconfig file
         * which isn't committed.
         */
        guard let publicKey = self.infoParameter("MARVEL_API_PUBLIC") else {
            throw APIError("No public API key")
        }
        guard let privateKey = self.infoParameter("MARVEL_API_PRIVATE") else {
            throw APIError("No private API key")
        }
        
        /*
         * See https://developer.marvel.com/documentation/authorization, what they call a
         * "timestamp" is really a nonce salt. I'll use UUID.
         */
        let ts = UUID().uuidString
        let nonce = "\(ts)\(privateKey)\(publicKey)"
        // force-unwrap next line: I'm confident the above string can be hashed
        let hash = Insecure.MD5.hash(data: nonce.data(using: .utf8)!).map {
            String(format: "%02x", $0)
        }.joined()
        // similarly force-unwrap here: if you can't build this URL I'll quickly discover that
        let listURL = URL(string:"https://gateway.marvel.com:443/v1/public/comics?apikey=\(publicKey)&ts=\(ts)&hash=\(hash)")!
        let task = URLSession.shared.dataTask(with: listURL) { data, response, error in
            if let data = data {
                do {
                    let model = try ComicCollectionModel.decodeModel(from: data)
                    DispatchQueue.main.async {
                        self.model = model
                        self.comics.forEach { comic in
                            comic.fetchThumbnail()
                        }
                    }
                } catch {
                    print("Unable to decode model from response! \(error)")
                }
            }
        }
        task.resume()
    }
}

@main
struct marvellousApp: App {
    var comicsCollection: ComicCollectionPresentation
    init() {
        comicsCollection = ComicCollectionPresentation()
        do {
            try comicsCollection.fetchComicsList()
        } catch {
            print("Unable to fetch list of comics! \(error)")
        }
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView(collection: comicsCollection)
        }
    }
}
