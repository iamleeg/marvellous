//
//  ComicCollectionModelTests.swift
//  marvellousTests
//
//  Created by Graham Lee on 25/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import XCTest
@testable import marvellous

class ComicCollectionModelTests: XCTestCase {
    func testGeneratingComicsListFromSampleJson() throws {
        let sampleJsonLocation = Bundle(for: ComicModelTests.self).url(forResource: "sample_call", withExtension: "json")
        guard let jsonURL = sampleJsonLocation else {
            XCTFail("Can't find the sample_call.json file")
            return
        }
        let jsonData = try Data(contentsOf: jsonURL)
        let model = try ComicCollectionModel.decodeModel(from: jsonData)
        XCTAssertEqual(model.data.results.count, 20)
        let firstComic = model.data.results[0]
        XCTAssertEqual(firstComic.title, "Marvel Previews (2017)")
        XCTAssertEqual(firstComic.id, 82967)
        XCTAssertEqual(firstComic.thumbnail.extension, "jpg")
        XCTAssertEqual(firstComic.thumbnail.path, "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
        
        let lastComic = model.data.results.last!
        XCTAssertEqual(lastComic.title, "Ant-Man (2003) #2")
        XCTAssertEqual(lastComic.id, 323)
        XCTAssertEqual(lastComic.thumbnail.extension, "jpg")
        XCTAssertEqual(lastComic.thumbnail.path, "http://i.annihil.us/u/prod/marvel/i/mg/f/20/4bc69f33cafc0")
    }
}
