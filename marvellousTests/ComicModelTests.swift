//
//  marvellousTests.swift
//  marvellousTests
//
//  Created by Graham Lee on 22/03/2022.
//
/*
 This file is part of marvellous.
 marvellous is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 marvellous is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import XCTest
import Foundation

@testable import marvellous

class ComicModelTests: XCTestCase {
    func testGeneratingModelFromSampleJsonResponse() throws {
        let sampleJsonLocation = Bundle(for: ComicModelTests.self).url(forResource: "sample_comic", withExtension: "json")
        guard let jsonURL = sampleJsonLocation else {
            XCTFail("Can't find the sample_comic.json file")
            return
        }
        let jsonData = try Data(contentsOf: jsonURL)
        let model = try ComicModel.decodeModel(from: jsonData)
        XCTAssertEqual(model.title, "Marvel Previews (2017)")
        XCTAssertEqual(model.id, 82967)
        XCTAssertEqual(model.thumbnail.extension, "jpg")
        XCTAssertEqual(model.thumbnail.path, "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
        XCTAssertEqual(model.series.name, "Marvel Previews (2017 - Present)")
    }
}
